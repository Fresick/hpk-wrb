package de.lab4inf.wrb.visitors;

import java.util.HashMap;
import java.util.List;

import de.lab4inf.wrb.Function;
import de.lab4inf.wrb.MathBaseVisitor;
import de.lab4inf.wrb.MathParser.AdditionContext;
import de.lab4inf.wrb.MathParser.AssignContext;
import de.lab4inf.wrb.MathParser.CallContext;
import de.lab4inf.wrb.MathParser.DefinitionContext;
import de.lab4inf.wrb.MathParser.DivisionContext;
import de.lab4inf.wrb.MathParser.ForgetContext;
import de.lab4inf.wrb.MathParser.ModuloContext;
import de.lab4inf.wrb.MathParser.MultiplicationContext;
import de.lab4inf.wrb.MathParser.NumberContext;
import de.lab4inf.wrb.MathParser.ParenthesesContext;
import de.lab4inf.wrb.MathParser.PowerContext;
import de.lab4inf.wrb.MathParser.PrefixContext;
import de.lab4inf.wrb.MathParser.ScientificContext;
import de.lab4inf.wrb.MathParser.ScriptContext;
import de.lab4inf.wrb.MathParser.VariableContext;
import de.lab4inf.wrb.WRBScript;

public class MathScriptVisitor extends MathBaseVisitor<Double> {

	private WRBScript script;

	private HashMap<String, Double> variables;

	public MathScriptVisitor(WRBScript script) {
		this.script = script;
		WRBScript.defaultVariables = new HashMap<>();
		script.globalVariables = new HashMap<>();

		script.functionDefinitions = new HashMap<>();
		WRBScript.defaultFuncs = new HashMap<>();
	}

	public MathScriptVisitor(WRBScript script, List<String> argnames, double[] args) {
		this.script = script;
		variables = new HashMap<>();
		for (int i = 0; i < argnames.size(); i++) {
			variables.put(argnames.get(i), args[i]);
		}
	}

	@Override
	public Double visitScript(ScriptContext ctx) {
		double res = super.visitScript(ctx);
		WRBScript.defaultVariables.put("ans", res);
		return res;
	}

	@Override
	protected Double aggregateResult(Double aggregate, Double nextResult) {
		if (nextResult == null)
			return aggregate;
		return super.aggregateResult(aggregate, nextResult);
	}

	@Override
	public Double visitCall(CallContext ctx) {
		HashMap<String, Function> functionDefinitions = getFunctions();
		if (!functionDefinitions.keySet().contains(ctx.name.getText())) {
			throw new IllegalArgumentException("Function \'" + ctx.name.getText() + "()\' does not exist!");
		}
		int numParams = ctx.statement().size();
		double[] params = new double[numParams];
		for (int i = 0; i < numParams; i++) {
			params[i] = ctx.statement(i).accept(this);
		}
		return functionDefinitions.get(ctx.name.getText()).eval(params);
	}

	@Override
	public Double visitMultiplication(MultiplicationContext ctx) {
		return ctx.fac1.accept(this) * ctx.fac2.accept(this);
	}

	@Override
	public Double visitModulo(ModuloContext ctx) {
		return ctx.num1.accept(this) % ctx.num2.accept(this);
	}
	
	@Override
	public Double visitNumber(NumberContext ctx) {
		return Double.parseDouble(ctx.num.getText());
	}

	@Override
	public Double visitScientific(ScientificContext ctx) {
		if (ctx.sign.getText().equals("+"))
			return Double.parseDouble(ctx.coef.getText()) * Math.pow(10, Integer.parseInt(ctx.expo.getText()));
		else
			return Double.parseDouble(ctx.coef.getText()) * Math.pow(10, - Integer.parseInt(ctx.expo.getText()));
	}

	@Override
	public Double visitAddition(AdditionContext ctx) {
		if (ctx.PLUS().getText().equals("+")) {
			return ctx.sum1.accept(this) + ctx.sum2.accept(this);
		} else {
			return ctx.sum1.accept(this) - ctx.sum2.accept(this);
		}
	}

	@Override
	public Double visitAssign(AssignContext ctx) {
		if (WRBScript.defaultVariables.keySet().contains(ctx.VARIABLE().getText())) {
			throw new IllegalArgumentException(
					ctx.VARIABLE().getText() + " is a default variable and can't be changed!");
		}
		double var = ctx.value.accept(this);
		addGlobalVariable(ctx.VARIABLE().getText(), var);
		return var;
	}

	@Override
	public Double visitVariable(VariableContext ctx) {
		if(variables != null && variables.containsKey(ctx.var.getText())) {
			return variables.get(ctx.var.getText());
		}
		HashMap<String, Double> variables = getVariables();
		if (!variables.keySet().contains(ctx.var.getText())) {
			throw new IllegalArgumentException("Variable \'" + ctx.var.getText() + "\' not assigned!");
		}
		return variables.get(ctx.var.getText());
	}

	@Override
	public Double visitParentheses(ParenthesesContext ctx) {
		return ctx.content.accept(this);
	}

	@Override
	public Double visitPower(PowerContext ctx) {
		return Math.pow(ctx.base.accept(this), ctx.exponent.accept(this));
	}

	@Override
	public Double visitPrefix(PrefixContext ctx) {
		switch (ctx.sign.getText()) {
		case "+": {
			return ctx.content.accept(this);
		}
		case "-": {
			return -ctx.content.accept(this);
		}
		default: {
			throw new IllegalArgumentException("Prefix parse error? (" + ctx.sign.getText() + ")");
		}
		}
	}

	@Override
	public Double visitDivision(DivisionContext ctx) {
		double div = ctx.quo1.accept(this) / ctx.quo2.accept(this);
		if (Double.isInfinite(div)) {
			throw new IllegalArgumentException("Division by 0!");
		}
		return div;
	}
	
	@Override
	public Double visitForget(ForgetContext ctx) {
		String varText = ctx.VARIABLE().getText();
		
		if(script.globalVariables.containsKey(varText)) {
			script.globalVariables.remove(varText);
		}
		
		return WRBScript.defaultVariables.get("ans");
	}

	@Override
	public Double visitDefinition(DefinitionContext ctx) {
		if (WRBScript.defaultFuncs.keySet().contains(ctx.name.getText())) {
			throw new IllegalArgumentException(ctx.name.getText() + " is a default function and can't be changed!");
		}
		List<String> paramNames = ctx.params.accept(new MathParamsVisitor());
//		for (String pn : paramNames) {
//			if (script.globalVariables.containsKey(pn)) {
//				throw new IllegalArgumentException(
//						pn + " is a set variable and can not be used as a function parameter");
//			}
//			if (WRBScript.defaultVariables.containsKey(pn)) {
//				throw new IllegalArgumentException(
//						pn + " is a default variable and can not be used as a function parameter");
//			}
//		}
		script.functionDefinitions.put(ctx.name.getText(), ctx.accept(new MathFunctionVisitor(script, paramNames)));
		return WRBScript.defaultVariables.get("ans");
	}

	public HashMap<String, Double> getVariables() {
		HashMap<String, Double> allVars = new HashMap<>();
		if(variables != null)
			allVars.putAll(variables);
		allVars.putAll(WRBScript.defaultVariables);
		allVars.putAll(script.globalVariables);
		return allVars;
	}

	public HashMap<String, Function> getFunctions() {
		HashMap<String, Function> allFuncs = new HashMap<>();
		allFuncs.putAll(script.functionDefinitions);
		allFuncs.putAll(WRBScript.defaultFuncs);
		return allFuncs;
	}

	public void addVariable(String name, double value) {
		variables.put(name, value);
	}

	public void addGlobalVariable(String name, double value) {
		script.globalVariables.put(name, value);
	}

	public void addFunction(String name, Function fct) {
		script.functionDefinitions.put(name, fct);
	}

	public HashMap<String, Double> getGlobalVariables() {
		return script.globalVariables;
	}

}

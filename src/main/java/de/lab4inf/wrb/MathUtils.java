package de.lab4inf.wrb;

public class MathUtils {

	public static double max(double... args) {
		if (args.length == 0 || args == null) {
			throw new IllegalArgumentException("No numbers were provided!");
		}
		double max = args[0];
		for (int i = 1; i < args.length; i++) {
			max = Math.max(max, args[i]);
		}
		return max;
	}

	public static double min(double... args) {
		if (args.length == 0 || args == null) {
			throw new IllegalArgumentException("No numbers were provided!");
		}
		double min = args[0];
		for (int i = 1; i < args.length; i++) {
			min = Math.min(min, args[i]);
		}
		return min;
	}

}

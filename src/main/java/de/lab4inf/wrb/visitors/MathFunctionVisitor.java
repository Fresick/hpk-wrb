package de.lab4inf.wrb.visitors;

import java.util.List;

import de.lab4inf.wrb.Function;
import de.lab4inf.wrb.MathBaseVisitor;
import de.lab4inf.wrb.MathParser.DefinitionContext;
import de.lab4inf.wrb.WRBFunction;
import de.lab4inf.wrb.WRBScript;

public class MathFunctionVisitor extends MathBaseVisitor<Function> {

	private WRBScript script;
	private List<String> paramNames;
	
	public MathFunctionVisitor(WRBScript script, List<String> paramNames) {
		super();
		this.script = script;
		this.paramNames = paramNames;
	}

	@Override
	public Function visitDefinition(DefinitionContext ctx) {
		return new WRBFunction(script, paramNames, ctx.functree);
	}
	
	@Override
	protected Function aggregateResult(Function aggregate, Function nextResult) {
		if(nextResult == null) {
			return aggregate;
		}
		return super.aggregateResult(aggregate, nextResult);
	}
	
}

package de.lab4inf.wrb;

import de.lab4inf.wrb.gui.PlotPanel;

public class TestApplication {

	private static final String testQuery = "f(x)=1/x";
	
	public static void main(String[] args) {
		try {
			WRBScript script = new WRBScript();
			System.out.println(script.parse(testQuery));
			PlotPanel.showPlot(script.getFunction("f"), "f");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package de.lab4inf.wrb;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AdditionalWRBScriptTest {

	final double eps = 1.E-8;
	Script script;

	@Before
	public final void setUp() throws Exception {
		script = getScript();
		assertNotNull("no script implementation", script);
	}

	protected Script getScript() {
		return new WRBScript();
	}

	@Test
	public void testSineOfPi() throws Exception {
		String task = "sin(pi)";
		assertEquals(0.0, script.parse(task), eps);
	}

	@Test
	public void testCreateFunction() throws Exception {
		String task = "f(x)=x^2;f(3)";
		assertEquals(9.0, script.parse(task), eps);
	}

	@Test
	public void testNegativeSqrt() throws Exception {
		String task = "sqrt(-2)";
		assertTrue(Double.isNaN(script.parse(task)));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testChangeDefaultFunction() throws Exception {
		String task = "sqrt(a)=a^(1/2)";
		script.parse(task);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testChangeDefaultVariable() throws Exception {
		String task = "pi=3.141";
		script.parse(task);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnknownSymbol() throws Exception {
		String task = "#=3.141";
		script.parse(task);
	}

	@Test
	public void testScriptConcat() throws Exception {
		String task = "tau=2*pi;C(r)=tau*r;A(r)=pi*r^2;x=5;a=10";
		script.parse(task);
		Script concatScript = getScript();
		concatScript = concatScript.concat(script);
		task = "C(9.3)+A(5.5)+pi+a+x";
		assertEquals(2 * Math.PI * 9.3 + Math.PI * Math.pow(5.5, 2) + Math.PI + 10 + 5, concatScript.parse(task), eps);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWronFunctionCall() throws Exception {
		String task = "f(x)=x^2;x=f(3);f(5.3, 3,1);";
		script.parse(task);
	}
}

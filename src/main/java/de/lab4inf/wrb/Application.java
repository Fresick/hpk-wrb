package de.lab4inf.wrb;

import java.util.Scanner;

import de.lab4inf.wrb.gui.PlotPanel;
import de.lab4inf.wrb.gui.PlotWindow;

public class Application {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String inStr = "";
		WRBScript script = new WRBScript();
		while (!(inStr = in.nextLine()).equals("exit")) {
			if(inStr.equals("plot")) {
				for(String s : script.functionDefinitions.keySet()) {
					PlotPanel.showPlot(script.functionDefinitions.get(s), s);
				}
				continue;
			}
			try {
				System.out.println(script.parse(inStr));
			} catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
			}
		}
		in.close();
		PlotWindow.killAll();
		System.exit(0);
	}
	
}

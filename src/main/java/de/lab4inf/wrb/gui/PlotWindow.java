package de.lab4inf.wrb.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class PlotWindow extends JFrame implements KeyListener, MouseWheelListener, MouseMotionListener {

	private static final long serialVersionUID = 2476204551487862015L;

	private static ArrayList<PlotWindow> instances = new ArrayList<>();
	
	private PlotPanel panel;
	
	public PlotWindow() {
		instances.add(this);
	}
	
	public PlotWindow(PlotPanel panel) {
		this();
		this.panel = panel;
		addMouseWheelListener(this);
		addKeyListener(this);
		addMouseMotionListener(this);
	}
	
	public static void killAll() {
		for(PlotWindow w : instances) {
			w.dispose();
		}
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		panel.scroll(e.getWheelRotation(), shift, ctrl);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	private boolean shift = false;
	private boolean ctrl = false;

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_SHIFT:
			shift = true;

		case KeyEvent.VK_CONTROL:
			ctrl = true;
		
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_SHIFT:
			shift = false;

		case KeyEvent.VK_CONTROL:
			ctrl = false;
		
		default:
			break;
		}
	}

	boolean dragging = false;
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(!dragging) {
			panel.startTranslation(e.getX(), e.getY());
			dragging = true;
		}
		panel.translate(e.getX(), e.getY());
		panel.replot();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		dragging = false;
	}

}

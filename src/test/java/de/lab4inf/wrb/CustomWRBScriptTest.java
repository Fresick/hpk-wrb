package de.lab4inf.wrb;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class CustomWRBScriptTest {

	final double eps = 1.E-8;
	Script script;

	@Before
	public final void setUp() throws Exception {
		script = getScript();
		assertNotNull("no script implementation", script);
	}

	protected Script getScript() {
		return new WRBScript();
	}

	@Test
	public void testSqrt() throws Exception {
		String task = "9^(1/2)";
		assertEquals(3.0, script.parse(task), eps);
	}

	@Test
	public void testVariableRoot() throws Exception {
		String task = "x=8";
		assertEquals(8.0, script.parse(task), eps);
		task = "-(x^(1/3))";
		assertEquals(-2.0, script.parse(task), eps);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDivByZero() throws Exception {
		String task = "10/0";
		script.parse(task);
	}

	@Test
	public void testAssignVariableWithVariable() throws Exception {
		String task = "x = 10; y = x; y;";
		assertEquals(10.0, script.parse(task), eps);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmpty() throws Exception {
		String task = "";
		script.parse(task);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTwoSemis() throws Exception {
		String task = "10*5;;x=3;";
		script.parse(task);
	}
	
	@Test
	public void testRandomNumbers() throws Exception {
		Random rand = new Random(System.nanoTime());
		Double a = rand.nextDouble();
		Double b = rand.nextDouble();
		Double c = rand.nextDouble();
		String task = a.toString() + "+" + b.toString() + "*(" + b.toString() + "^" + c.toString() + ")";
		assertEquals(a+b*(Math.pow(b, c)), script.parse(task), eps);
	}
}

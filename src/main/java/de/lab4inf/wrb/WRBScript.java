package de.lab4inf.wrb;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Set;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;

import de.lab4inf.wrb.MathParser.ScriptContext;
import de.lab4inf.wrb.listeners.MathLexerErrorListener;
import de.lab4inf.wrb.listeners.MathParserErrorListener;
import de.lab4inf.wrb.visitors.MathScriptVisitor;

import org.antlr.v4.runtime.CharStreams;

public class WRBScript implements Script {

	private MathScriptVisitor msv;

	public static HashMap<String, Double> defaultVariables;
	public HashMap<String, Double> globalVariables;
	
	public HashMap<String, Function> functionDefinitions;
	public static HashMap<String, Function> defaultFuncs;
	
	public WRBScript() {
		msv = new MathScriptVisitor(this);
		initDefaultFuncs();
		initDefaultVars();
	}

	private void initDefaultFuncs() {
		addDefaultFunc("abs", 1, params -> Math.abs(params[0]));

		addDefaultFunc("sqrt", 1, params -> Math.sqrt(params[0]));
		addDefaultFunc("cbrt", 1, params -> Math.cbrt(params[0]));

		addDefaultFunc("sin", 1, params -> Math.sin(params[0]));
		addDefaultFunc("asin", 1, params -> Math.asin(params[0]));
		addDefaultFunc("sinh", 1, params -> Math.sinh(params[0]));

		addDefaultFunc("cos", 1, params -> Math.cos(params[0]));
		addDefaultFunc("acos", 1, params -> Math.acos(params[0]));
		addDefaultFunc("cosh", 1, params -> Math.cosh(params[0]));

		addDefaultFunc("tan", 1, params -> Math.tan(params[0]));
		addDefaultFunc("atan", 1, params -> Math.atan(params[0]));
		addDefaultFunc("tanh", 1, params -> Math.tanh(params[0]));

		addDefaultFunc("max", WRBFunction.N_PARAMS, params -> MathUtils.max(params));
		addDefaultFunc("min", WRBFunction.N_PARAMS, params -> MathUtils.min(params));

		addDefaultFunc("exp", 1, params -> Math.exp(params[0]));
		addDefaultFunc("ln", 1, params -> Math.log(params[0]));
		addDefaultFunc("logE", 1, params -> Math.log(params[0]));
		addDefaultFunc("log10", 1, params -> Math.log10(params[0]));
		addDefaultFunc("log", 1, params -> Math.log10(params[0]));
		addDefaultFunc("log2", 1, params -> Math.log(params[0]) / Math.log(2));
		addDefaultFunc("lb", 1, params -> Math.log(params[0]) / Math.log(2));
		addDefaultFunc("ld", 1, params -> Math.log(params[0]) / Math.log(2));
		addDefaultFunc("pow", 2, params -> Math.pow(params[0], params[1]));
	}

	private void addDefaultFunc(String name, int paramCount, Function f) {
		WRBScript.defaultFuncs.put(name, new WRBFunction(paramCount, f));
	}

	private void initDefaultVars() {
		addDefaultVariable("ans", 0.0);
		
		addDefaultVariable("pi", Math.PI);
		addDefaultVariable("e", Math.E);
	}

	private void addDefaultVariable(String name, double val) {
		WRBScript.defaultVariables.put(name, val);
	}
	
	public double parse(String definition) {
		MathLexer ml = new MathLexer(CharStreams.fromString(definition));
		ml.removeErrorListener(ConsoleErrorListener.INSTANCE);
		ml.addErrorListener(new MathLexerErrorListener());

		MathParser mp = new MathParser(new CommonTokenStream(ml));
		mp.removeErrorListener(ConsoleErrorListener.INSTANCE);
		mp.addErrorListener(new MathParserErrorListener());

		ScriptContext script = mp.script();

		ml.reset();

		if (ml.getAllTokens().size() != script.getSourceInterval().length()) {
			throw new IllegalArgumentException("Syntax error! Not everything was parsed!");
		}

		// System.out.println(script.toStringTree(mp));

		return script.accept(msv);
	}

	public double parse(InputStream defStream) throws IOException {
		int curr = 0;
		String task = "";
		while((curr = defStream.read()) != -1){
			task += (char) curr;
		}
		return parse(task);
	}

	public Set<String> getFunctionNames() {
		return msv.getFunctions().keySet();
	}

	public Set<String> getVariableNames() {
		return msv.getVariables().keySet();
	}

	public void setFunction(String name, Function fct) {
		msv.addFunction(name, fct);
	}

	public Function getFunction(String name) {
		Function f = msv.getFunctions().get(name);
		if(f == null) throw new IllegalArgumentException("Function not found!");
		return f;
	}

	public double getVariable(String name) {
		if (!msv.getVariables().keySet().contains(name)) {
			throw new IllegalArgumentException("Unknown variable \'" + name + "\'!");
		}
		return msv.getVariables().get(name);
	}

	public void setVariable(String name, double value) {
		globalVariables.put(name, value);
	}

	@Override
	public Script concat(Script that) {
		if (!(that instanceof WRBScript)) {
			return Script.super.concat(that);
		}

		WRBScript newScript = new WRBScript();
		WRBScript wrbThat = (WRBScript) that;

		for (String var : this.getVariableNames()) {
			newScript.setVariable(var, this.getVariable(var));
		}
		for (String var : wrbThat.getVariableNames()) {
			newScript.setVariable(var, wrbThat.getVariable(var));
		}

		HashMap<String, Double> thisGlobals = this.getMSV().getGlobalVariables();
		for (String var : thisGlobals.keySet()) {
			newScript.getMSV().addGlobalVariable(var, thisGlobals.get(var));
		}
		HashMap<String, Double> thatGlobals = wrbThat.getMSV().getGlobalVariables();
		for (String var : thatGlobals.keySet()) {
			newScript.getMSV().addGlobalVariable(var, thatGlobals.get(var));
		}

		for(String func : this.getFunctionNames()) {
			newScript.setFunction(func, this.getFunction(func));
		}
		for(String func : that.getFunctionNames()) {
			newScript.setFunction(func, that.getFunction(func));
		}
		
		return newScript;
	}

	private MathScriptVisitor getMSV() {
		return this.msv;
	}

}

package de.lab4inf.wrb;

import java.util.ArrayList;
import java.util.List;

import de.lab4inf.wrb.Function;
import de.lab4inf.wrb.MathParser.StatementContext;
import de.lab4inf.wrb.visitors.MathScriptVisitor;

public class WRBFunction implements Function {

	public static final int N_PARAMS = -1;

	protected List<String> params;
	private Function f;
	private boolean ignoreParamCount = false;

	public WRBFunction(WRBScript script, List<String> params, StatementContext functree) {
		this.params = params;
		this.f = args -> functree.accept(new MathScriptVisitor(script, params, args));
	}

	public WRBFunction(int paramCount, Function f) {
		this.params = new ArrayList<>();
		ignoreParamCount = paramCount == N_PARAMS;
		for (int i = 0; i < paramCount; i++) {
			params.add(Integer.toString(i));
		}
		this.f = f;
	}

	@Override
	public double eval(double... args) {
		if (!ignoreParamCount && args.length != params.size()) {
			throw new IllegalArgumentException(
					"Called function got " + args.length + " arguments and only accepts EXACTLY " + params.size());
		}
		return f.eval(args);
	}

}

grammar Math;

@header {
  package de.lab4inf.wrb;
}

WS: [ \t\n\r]+ -> skip;

INTEGER: ([0-9]+);
FLOAT: INTEGER? '.' INTEGER?;
scientific: coef=FLOAT 'e' sign=PLUS? expo=INTEGER;

VARIABLE: [a-zA-Z]+ INTEGER?;

PLUS: '+'|'-';
TIMES: '*';
OVER: '/';
POWER: '^' | TIMES TIMES;

OP: '(';
CP: ')';
EQ: '=';
SEMI: ';';
COMMA: ',';
QM: '?';
PERCENT: '%';

statement: num=INTEGER #number
| num=FLOAT #number
| num=scientific #sci
| var=VARIABLE #variable
| sign=PLUS content=statement #prefix
| OP content=statement CP #parentheses
| <assoc=right>base=statement POWER exponent=statement #power
| quo1=statement OVER quo2=statement #division
| fac1=statement TIMES fac2=statement #multiplication
| num1=statement PERCENT num2=statement #modulo
| sum1=statement PLUS sum2=statement #addition
| name=VARIABLE OP statement (COMMA statement)* CP #call;

paramlist: VARIABLE (COMMA VARIABLE)* #params;

function: name=VARIABLE OP params=paramlist CP EQ functree=statement #definition;

varassign: VARIABLE EQ value=statement #assign;

remove: VARIABLE QM #forget;

subscript: function
| remove
| varassign
| statement;

script: subscript(SEMI subscript)*SEMI?;


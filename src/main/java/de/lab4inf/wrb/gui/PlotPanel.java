package de.lab4inf.wrb.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.lab4inf.wrb.Function;

public class PlotPanel extends JPanel {

	private static final long serialVersionUID = -750233393608877859L;

	private BufferedImage plot;

	private double xcenter;
	private double xrange;
	private double ycenter;
	private double yrange;

	public PlotPanel(int width, int height, double xcenter, double xrange, double ycenter, double yrange) {
		this.xcenter = xcenter;
		this.xrange = xrange;
		this.ycenter = ycenter;
		this.yrange = yrange;

		plot = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		clearPlot();
	}

	private void clearPlot() {
		for (int i = 0; i < getPreferredSize().width; i++) {
			for (int j = 0; j < getPreferredSize().height; j++) {
				plot.setRGB(j, i, Color.white.getRGB());
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(plot, null, null);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(plot.getWidth(), plot.getHeight());
	}

	public double getX(int xPx) throws IndexOutOfBoundsException {
		if (xPx < 0 || xPx > getPreferredSize().width) {
			throw new IndexOutOfBoundsException();
		}
		return (xrange / getPreferredSize().width) * xPx + xcenter - (xrange / 2.0);
	}

	public double getY(int yPx) throws IndexOutOfBoundsException {
		if (yPx < 0 || yPx > getPreferredSize().height) {
			throw new IndexOutOfBoundsException();
		}
		return yrange - (yrange / getPreferredSize().height) * yPx + ycenter - (yrange / 2.0);
	}

	public int getYPx(double yVal) {
		double m = getPreferredSize().height / yrange;
		double n = -m * ycenter + (m * yrange / 2);
		return getPreferredSize().height - (int) Math.floor(m * yVal + n);
	}

	public int getXPx(double xVal) {
		double m = getPreferredSize().width / xrange;
		double n = -m * xcenter + (m * xrange / 2);
		return (int) Math.floor(m * xVal + n);
	}

	public double getUpPxX() {
		return xrange / getPreferredSize().width;
	}

	public double getUpPxY() {
		return yrange / getPreferredSize().height;
	}

	private Function f;

	public void plot(Function f) {
		this.f = f;
		Point[] points = new Point[getPreferredSize().width];
		Graphics g = plot.getGraphics();

		drawScale(g);

		g.setColor(Color.RED);
		for (int i = 0; i < getPreferredSize().width - 1; i++) {
			try {
				points[i] = new Point(i, getYPx(f.eval(getX(i))));
			} catch (Exception e) {
				points[i] = null;
			}
			if (i >= 1 && i < getPreferredSize().width - 1) {
				if (points[i] == null || points[i - 1] == null) {
					continue;
				}
				g.drawLine(points[i].x, points[i].y, points[i - 1].x, points[i - 1].y);
			}
		}

		g.dispose();
		repaint();
	}

	private void drawScale(Graphics g) {
		g.setColor(new Color(180, 180, 180));
		g.drawLine(0, getYPx(0.0), getPreferredSize().width - 1, getYPx(0.0));
		g.drawLine(getXPx(0.0), 0, getXPx(0.0), getPreferredSize().height - 1);

//		double xinterval = xrange / 10.0;
//		double xrinterval = 0.1;
//		if (xinterval > 1.0) {
//			xrinterval = Math.floor(xinterval);
//		}
//		int pi = (int) Math.abs(xrinterval * getUpPxX());
//		if (pi <= 0)
//			pi = 1;
//		int fxstart = getXPx(0.0);
//		int xistart = fxstart;
//		if (xistart >= 0) {
//			while (xistart >= 0) {
//				xistart -= pi;
//			}
//		} else {
//			while (xistart < 0) {
//				xistart += pi;
//			}
//			xistart -= pi;
//		}
//		int xi = xistart;
//
//		do {
//			int ypx = getYPx(0);
//			int xpx = xi;
//			g.drawLine(xpx, ypx, xpx, ypx - 7);
//
//			String numStr = Double.toString(xi);
//			g.drawString(numStr, xpx - 12, ypx + 15);
//			xi += xrinterval;
//		} while (xi <= xcenter + xrange / 2);
	}

	public void replot() {
		clearPlot();
		if (f != null) {
			plot(f);
		}
	}

	public static void showPlot(Function f, String title) {
		PlotPanel panel = new PlotPanel(500, 500, 0, 20, 0, 20);
		PlotWindow frame = new PlotWindow(panel);
		frame.setTitle(title);
		frame.add(panel);
		frame.pack();
		frame.setLocation(200, 200);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		panel.plot(f);
	}

	private static final double fac = 100.0;

	public void scroll(int wheelRotation, boolean shift, boolean ctrl) {
		if (!shift && !ctrl) {
			xrange += wheelRotation * getUpPxX() * fac;
			yrange += wheelRotation * getUpPxY() * fac;
		} else if (shift) {
			xrange += wheelRotation * getUpPxX() * fac;
		} else if (ctrl) {
			yrange += wheelRotation * getUpPxY() * fac;
		}
		replot();
	}

	private Point lastTrans;

	public void translate(int x, int y) {
		xcenter -= (x - lastTrans.x) * getUpPxX();
		ycenter += (y - lastTrans.y) * getUpPxY();
		startTranslation(x, y);
	}

	public void startTranslation(int x, int y) {
		lastTrans = new Point(x, y);
	}
}

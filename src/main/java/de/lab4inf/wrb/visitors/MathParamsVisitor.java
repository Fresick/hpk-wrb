package de.lab4inf.wrb.visitors;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.TerminalNode;

import de.lab4inf.wrb.MathBaseVisitor;
import de.lab4inf.wrb.MathParser.ParamsContext;

public class MathParamsVisitor extends MathBaseVisitor<List<String>> {

	
	@Override
	public List<String> visitParams(ParamsContext ctx) {
		ArrayList<String> params = new ArrayList<>();
		for(TerminalNode var : ctx.VARIABLE()) {
			params.add(var.getText());
		}
		return params;
	}
	
	@Override
	protected List<String> aggregateResult(List<String> aggregate, List<String> nextResult) {
		if(nextResult == null) {
			return aggregate;
		}
		return super.aggregateResult(aggregate, nextResult);
	}
	
}
